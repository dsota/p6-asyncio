import sdp_transform
import asyncio
import json

class EchoServerProtocol:
    def connection_made(self, transport):
        self.transport = transport

    def datagram_received(self, data, addr):
        _data = data.decode()
        print('Received %r from %s' % (_data, addr))
        json_sdp_offer = json.loads(_data)
        sdp = sdp_transform.parse(json_sdp_offer['sdp'])
        sdp['media'][0]["port"] = 34543
        sdp['media'][1]["port"] = 34543
        json_sdp_answer =json.dumps({"type": "answer", "sdp": sdp_transform.write(sdp)})
        print('Send %r to %s' % (json_sdp_answer, addr))
        self.transport.sendto(json_sdp_answer.encode(), addr)


async def main():
    print("Starting UDP server")

    # Get a reference to the event loop as we plan to use
    # low-level APIs.
    loop = asyncio.get_running_loop()

    # One protocol instance will be created to serve all
    # client requests.
    transport, protocol = await loop.create_datagram_endpoint(
        lambda: EchoServerProtocol(),
        local_addr=('127.0.0.1', 9991))

    try:
        await asyncio.sleep(3600)  # Serve for 1 hour.
    finally:
        transport.close()


asyncio.run(main())